<?php

/**
 * @file
 * Business Yellow Theme
 * Created by Zyxware Technologies
 */

?>
<div id="mainwrapper" class="container-12">
  <div id="header" class="grid-12">
    <div id="logo_menu_wrapper">
      <div id="logo_section" class="grid-3 alpha clearfix">
        <?php if ($site_name): ?>
          <a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>">
            <img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" />
          </a>
        <?php endif; ?>
      </div>
      <div id="icons_outer" class="grid-4">
        <?php if ($page ['icons']): ?>
          <div class="socialmedia_icons">
            <?php print render($page['icons']); ?>
          </div>
        <?php endif; ?>
      </div>
      <div id="nav" class="grid-12 clearfix alpha omega">
        <?php if ($page ['primarynav']): ?>
          <div class="grid-7 alpha">
            <?php print render($page['primarynav']); ?>
          </div>
        <?php endif; ?>
        <?php if ($page ['search']): ?>
          <div id="search" class="grid-5 omega">
            <?php print render($page['search']); ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
    <div id="steps3_outer" class="grid-12">
      <div class="arrow1"></div>
      <div class="arrow2"></div>

      <?php if ($page ['stepone']): ?>
        <div id="stepone" class="grid-4 alpha">
          <?php print render($page['stepone']); ?>
        </div>
      <?php endif; ?>

      <?php if ($page ['steptwo']): ?>
        <div id="steptwo" class="grid-4 alpha">
          <?php print render($page['steptwo']); ?>
        </div>
      <?php endif; ?>
      <?php if ($page ['stepthree']): ?>
        <div id="stepthree" class="grid-4 alpha">
          <?php print render($page['stepthree']); ?>
        </div>
      <?php endif;?>
    </div>
  </div>
  <div id="middlesection_outer" class="grid-12 clearfix">
    <div id="middle_inner2" class="grid-12 alpha omega">
      <?php if ($page ['content']): ?>
        <?php print render($page['content']); ?>
      <?php endif; ?>
    </div>

    <div id="middle_lower" class="grid-11">
      <?php if ($page ['leftbar']): ?>
        <div id="leftbar_home" class="grid-3" style="width:200px;">
          <?php print render($page['leftbar']); ?>
        </div>
      <?php endif; ?>
      <?php if ($page ['rightbar']): ?>
        <div id="rightbar_home" class="grid-8 alpha">
          <?php print render($page['rightbar']); ?>
        </div>
      <?php endif; ?>
      <?php if ($page ['rightblock1']): ?>
        <div id="rightblock1 " class="grid-4">
          <?php print render($page['rightblock1']); ?>
        </div>
      <?php endif; ?>
      <?php if ($page ['rightblock2']): ?>
        <div id="rightblock2" class="grid-4 omega">
          <?php print render($page['rightblock2']); ?>
        </div>
      <?php endif; ?>
      <?php if ($page ['rightblock3']): ?>
        <div id="rightblock3" class="grid-4 omega">
          <?php print render($page['rightblock3']); ?>
        </div>
      <?php endif; ?>
      <?php if ($page ['rightblock4']): ?>
        <div id="rightblock4" class="grid-4 omega">
          <?php print render($page['rightblock4']); ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
  <div id="footer" class="grid-12 clearfix">
    <div class="yellowborder_wrap">
      <?php if ($page ['footerleft']): ?>
        <div id="footer_left" class="grid-4 omega">
          <?php print render($page['footerleft']); ?>
        </div>
      <?php endif; ?>
      <div id="footer_middle" class="grid-4 omega">
        <?php if ($page ['footermiddle']): ?>

          <?php print render($page['footermiddle']); ?>

        <?php endif; ?>

      </div>
      <?php if ($page ['footerright']): ?>
        <div id="footer_right" class="grid-4 omega">
          <?php print render($page['footerright']); ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
  <div class="footer_zyxware">
    Theme by <a href="http://www.zyxware.com/">Zyxware Technologies</a>
  </div>
</div>
