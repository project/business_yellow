WHAT IS Business Yellow?
------------------------

Business Yellow is a business style theme easy accessible and clean design with a professional outlook, div based, fixed width, three column layout.

INSTALLATION
------------

 1. Download Business Yellow from http://drupal.org/project/business_yellow

 2. Unpack the downloaded files, take the folders and place them in your
    Drupal installation under one of the following locations:
      sites/all/themes
        making it available to the default Drupal site and to all Drupal sites
        in a multi-site configuration
      sites/default/themes
        making it available to only the default Drupal site
      sites/example.com/themes
        making it available to only the example.com site if there is a
        sites/example.com/settings.php configuration file

    Note: you will need to create the "themes" folder under "sites/all/"
    or "sites/default/".

FURTHER READING
---------------

Full documentation on using Business Yellow:
  http://www.freedrupalthemes.net/docs/business_yellow

Drupal theming documentation in the Theme Guide:
  http://drupal.org/theme-guide
  
Business Yellow demo site
  http://d7.freedrupalthemes.net/t/business_yellow
